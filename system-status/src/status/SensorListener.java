package status;

import java.util.Arrays;

import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;

public class SensorListener implements ServiceListener{

	@Override
	public void serviceChanged(ServiceEvent event) {
		
		ServiceReference ref = event.getServiceReference();
		
		String[] objectClass = (String[])ref.getProperty("objectClass");
		System.out.println("objectClass: " + Arrays.toString(objectClass) + ", eventType: " + event.getType());
		
	}

}
