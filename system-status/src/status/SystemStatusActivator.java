package status;

import java.util.Collection;
import java.util.Locale;
import java.util.ResourceBundle;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

import sensor.api.Sensor;

public class SystemStatusActivator implements BundleActivator, Runnable {

	BundleContext context;

	@Override
	public void start(BundleContext context) throws Exception {
		System.out.println("system status bundle started");
		this.context = context;
		context.addServiceListener(new SensorListener());
		new Thread(this).start();
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		System.out.println("system status bundle stopped");
		this.context = null;
	}

	private void printSensorValue(Sensor sensor) {

		Locale.setDefault(new Locale("pl", "PL"));
		// en, US
		// sensors_en_US.properties
		// sensors_en.properties
		// sensors.properties
		ResourceBundle resourceBundle = ResourceBundle.getBundle("sensors");
		String label = resourceBundle.getString("sensors." + sensor.getType());
		System.out.println("sensor of type [" + label + "] value[" + sensor.getValue() + "]");
	}

	@Override
	public void run() {

		try {
			while (context != null) {
				Collection<ServiceReference<Sensor>> refs = context.getServiceReferences(Sensor.class, null);
				for (ServiceReference<Sensor> ref : refs) {
					Sensor sensor = context.getService(ref);
					printSensorValue(sensor);
				}

				if (refs == null || refs.isEmpty()) {
					System.out.println("sensor service not found");
				}
				Thread.sleep(5*1000);
			}
		} catch (InvalidSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
