package sensor.api;

public interface Sensor {
	
	String getType();
	
	String getValue();

}
