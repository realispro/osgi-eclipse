package sensor.time;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import sensor.api.Sensor;

public class TimeSensor implements Sensor {

	@Override
	public String getType() {
		return "TIME";
	}

	@Override
	public String getValue() {
		return LocalTime.now().format(DateTimeFormatter.ISO_TIME);
	}

}
