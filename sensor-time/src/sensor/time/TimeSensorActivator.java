package sensor.time;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import sensor.api.Sensor;

public class TimeSensorActivator implements BundleActivator{
	
	private ServiceRegistration<Sensor> serviceRegistration;

	@Override
	public void start(BundleContext context) throws Exception {

		System.out.println("activating time sensor service");
		serviceRegistration = context.registerService(Sensor.class, new TimeSensor(), null);		
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		System.out.println("deactivating time sensor service.");
		serviceRegistration.unregister();		
	}

}
