package sensor.lang;

import java.util.Locale;

import sensor.api.Sensor;

public class LangSensor implements Sensor {

	@Override
	public String getType() {
		return "LANG";
	}

	@Override
	public String getValue() {
		return Locale.getDefault().getLanguage();
	}

}
